$(document).ready(function(){
    console.log( "ready!" );
    $('.slider').slick({
        arrows: true,
        dots: true,
        slidesToShow: 4,
        slidesToScroll: 4,
        speed: 600,
        infinite: false,
        initialSlide: 0,
        responsive: [
            {
                breakpoint: 920,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            }
        ]   
    });
});